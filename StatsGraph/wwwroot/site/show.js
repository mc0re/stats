﻿
var initChartData = {
	type: 'bar',
	label: 'Sample chart',
	labels: ['January', 'February', 'March', 'April', 'May', 'June'],
	datasets: [{
		label: 'Dataset 1',
		backgroundColor: 'rgba(255, 0, 0, 0.5)',
		borderColor: 'rgba(255, 0, 0, 1)',
		borderWidth: 1,
		data: [
			1, 3, 5, 2, 4, 6
		]
	}, {
		label: 'Dataset 2',
		backgroundColor: 'rgba(0, 0, 255, 0.5)',
		borderColor: 'rgba(0, 0, 255, 1)',
		borderWidth: 1,
			data: [
			9, 8, 7, 6, 5, 4
		]
	}]

};


function createChart(chartData) {
	var ctx = document.getElementById('canvas').getContext('2d');

	if (window.chart) {
		window.chart.destroy();
	}
	window.chart = new Chart(ctx, {
		type: chartData.type,
		data: chartData,
		options: {
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: true,
				text: chartData.label
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});
}


window.onload = function () {
	var input = this.document.getElementById('data-input');
	input.value = JSON.stringify(initChartData, null, 4);

	this.createChart(initChartData);
	var upd = document.getElementById('update-btn');
	upd.onclick = function () {
		try {
			var chartData = JSON.parse(input.value);
			createChart(chartData);
		} catch (e) {
			alert(e);
		}
	}
};
