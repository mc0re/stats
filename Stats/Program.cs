﻿using StatsData;


namespace Stats
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var rows = new DataRepository().Get();

            Work.ShowIncomeVsEducation(rows);

            //Examples.ShowAllStats(rows);
            //Examples.ShowLeadershipAsPie(rows);
            //Examples.ShowTopIncomeAsBar(rows);
            //Examples.ShowIncomeForEducationScatter(rows);
            //Examples.ShowIncomeForEducationBubble(rows);
        }
    }
}
