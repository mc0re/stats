﻿using ChartJsLibrary;
using StatsData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Stats
{
    internal class Work
    {
        internal static void ShowIncomeVsEducation(ICollection<DataRow> rows)
        {
            var model = new ChartDataModel(ChartTypes.Bar, "Income vs education");

            var data =
                from r in rows
                let x = (r.MinEducation + r.MaxEducation) / 2
                group r by x into g
                orderby g.Key
                select new
                {
                    Education = g.Key,
                    Income = g.Average(line => line.MinOwnIncome)
                };

            var set = model.AddSet("Set 1");
            foreach (var point in data)
            {
                set.AddPoint(point.Education, point.Income);
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }
    }
}