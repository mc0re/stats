﻿using ChartJsLibrary;
using StatsData;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;


namespace Stats
{
    public static class Examples
    {
        public static void ShowAllStats(ICollection<DataRow> rows)
        {
            Console.WriteLine($"{rows.Count} entries.");

            ShowStats(rows, r => r.Gender);
            ShowStats(rows, r => r.AgeGroup);
            ShowStats(rows, r => r.School);
            ShowStats(rows, r => r.Education);
            ShowStats(rows, r => r.Vocational);
            ShowStats(rows, r => r.JobStatus);
            ShowStats(rows, r => r.LeadershipStatus);
            ShowStats(rows, r => r.HousingStatus);
            ShowStats(rows, r => r.HousingType);
            ShowStats(rows, r => r.Occupation);
            ShowStats(rows, r => r.OccupationGroup);
            ShowStats(rows, r => r.Location);
            ShowStats(rows, r => r.HouseholdIncome);
            ShowStats(rows, r => r.OwnIncome);
            ShowStats(rows, r => r.LocationAmt);
            ShowStats(rows, r => r.CivilStatus);
            ShowStats(rows, r => r.Batch);
            ShowStats(rows, r => r.Year);
            ShowStats(rows, r => r.Month.HasValue ? r.Month.ToString() : "unknown");
            ShowStats(rows, r => r.Day ?? -1);
        }


        private static void ShowStats<T>(ICollection<DataRow> rows, Func<DataRow, T> selector) where T : IConvertible
        {
            var dict = new ConcurrentDictionary<T, List<DataRow>>();

            foreach (var row in rows)
            {
                var bucketId = selector.Invoke(row);
                var bucket = dict.GetOrAdd(bucketId, _ => new List<DataRow>());
                bucket.Add(row);
            }

            var answered =
                selector.Invoke(rows.First()).GetType().IsClass
                ? rows.Where(r => selector.Invoke(r) != null).Count()
                : rows.Where(r => Convert.ToInt32(selector.Invoke(r)) > 0).Count();

            Console.WriteLine("---");
            foreach (var bucketId in dict.Keys)
            {
                var value = dict[bucketId].Count;
                Console.WriteLine($"{bucketId}: {value} entries ({value / (double)answered:p})");
            }
        }


        public static void ShowTopIncomeAsBar(ICollection<DataRow> rows)
        {
            var model = new ChartDataModel(ChartTypes.Radar, "Top income vs geography");
            AddTopIncome(rows, 700_000, model);
            AddTopIncome(rows, 800_000, model);
            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }


        private static void AddTopIncome(ICollection<DataRow> rows, double maxOwn, ChartDataModel model)
        {
            var data = (
                from r in rows
                where r.MaxOwnIncome >= maxOwn
                group r by r.LocationAmt into g
                let inAmt = rows.Count(r2 => r2.LocationAmt == g.Key)
                let percent = g.Count() / (double)inAmt
                orderby g.Key
                select new { Percent = percent, Amt = g.Key }
                ).ToList();

            var set = model.AddSet($"{maxOwn} or more");
            foreach (var point in data)
            {
                set.AddPoint(point.Amt.ToString(), point.Percent * 100);
            }
        }


        public static void ShowLeadershipAsPie(ICollection<DataRow> rows)
        {
            var model = new ChartDataModel(ChartTypes.Pie, "Managers vs gender");

            var data = (
                from r in rows
                where r.LeadershipStatus == LeadershipStatuses.Leader || r.LeadershipStatus == LeadershipStatuses.NotLeader
                group r by r.Gender into g
                orderby g.Key
                select new
                {
                    Gender = g.Key,
                    Managers = g.Where(gr => gr.LeadershipStatus == LeadershipStatuses.Leader).Count(),
                    NotManagers = g.Where(gr => gr.LeadershipStatus == LeadershipStatuses.NotLeader).Count(),
                    Answered = g.Count()
                }
                ).ToDictionary(r => r.Gender);

            foreach (var item in data)
            {
                var set = model.AddSet(item.Key.ToString());
                var value = item.Value;
                set.AddPoint("Managers", value.Managers / (double)value.Answered * 100);
                set.AddPoint("Not managers", value.NotManagers / (double)value.Answered * 100);
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }


        public static void ShowIncomeForEducationScatter(ICollection<DataRow> rows)
        {
            var model = new ChartDataModel(ChartTypes.Scatter, "Average income vs education");
            var set = model.AddSet("Average income");

            var data = (
                from r in rows
                where r.MinEducation > 0 && r.MaxOwnIncome > 0
                let g = new
                {
                    Education = (r.MinEducation + r.MaxEducation) / 2.0,
                    Income = r.MinOwnIncome
                }
                group g by g.Education into e
                orderby e.Key
                select new
                {
                    Education = e.Key,
                    Income = e.Average(i => i.Income)
                }
                ).ToList();

            foreach (var p in data)
            {
                set.AddPoint(p.Education, p.Income);
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }


        public static void ShowIncomeForEducationBubble(ICollection<DataRow> rows)
        {
            var model = new ChartDataModel(ChartTypes.Bubble, "Income vs education");
            var set = model.AddSet("Income");

            var data = (
                from r in rows
                where r.MinEducation > 0 && r.MaxOwnIncome > 0
                let g = new
                {
                    Education = (r.MinEducation + r.MaxEducation) / 2.0,
                    Income = r.MinOwnIncome
                }
                group g by new { g.Education, g.Income } into e
                orderby e.Key.Education
                select new
                {
                    e.Key.Education,
                    e.Key.Income,
                    Count = e.Count()
                }
                ).ToList();

            foreach (var p in data)
            {
                set.AddPoint(p.Education, p.Income, p.Count);
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }
    }
}
