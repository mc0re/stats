# Stats

Basic statistics.

## Input data

Source: http://cssr.surveybanken.aau.dk/webview/

Data set: *Mandag Morgen*

## Example charts

### Managers vs gender

```json
{
  "type": "pie",
  "label": "Managers vs gender",
  "labels": [
    "Managers",
    "Not managers"
  ],
  "datasets": [
    {
      "label": "Female",
      "data": [
        19.022883616453743,
        80.97711638354626
      ],
      "backgroundColor": [
        "rgba(255, 170, 0, 0.7)",
        "rgba(0, 85, 255, 0.7)"
      ],
      "borderColor": "rgba(0, 0, 0, 1)",
      "borderWidth": 2
    },
    {
      "label": "Male",
      "data": [
        31.51829911547513,
        68.48170088452488
      ],
      "backgroundColor": [
        "rgba(255, 170, 0, 0.7)",
        "rgba(0, 85, 255, 0.7)"
      ],
      "borderColor": "rgba(0, 0, 0, 1)",
      "borderWidth": 2
    }
  ]
}
```

### Top income vs geogaphy

```json
{
  "type": "radar",
  "label": "Top income vs geography",
  "labels": [
    "KobenhavnFrederiksberg",
    "Kobenhavn",
    "Frederiksborg",
    "Roskilde",
    "Vestsjaelland",
    "Storstroem",
    "Bornholm",
    "Fyn",
    "Soenderjylland",
    "Ribe",
    "Vejle",
    "Rinkoebing",
    "Aarhus",
    "Viborg",
    "Nordjylland"
  ],
  "datasets": [
    {
      "label": "700000 or more",
      "data": [
        0.5465751831608326,
        0.8395644759281123,
        1.018047200370199,
        0.8124337689862239,
        0.30916245081506466,
        0.2528445006321113,
        0.7751937984496124,
        0.23387905111927834,
        0.19083969465648853,
        0.2508061626657112,
        0.3803131991051454,
        0.4299226139294927,
        0.43714555765595464,
        0.2363268062120189,
        0.2881844380403458
      ],
      "backgroundColor": "rgba(255, 170, 0, 0.4)",
      "borderColor": "rgba(255, 170, 0, 1)",
      "borderWidth": 2
    },
    {
      "label": "800000 or more",
      "data": [
        0.3139899988370741,
        0.4460186278368096,
        0.5321610365571495,
        0.4238784881667256,
        0.19673974142776843,
        0.12642225031605564,
        0.3875968992248062,
        0.11693952555963917,
        0.031806615776081425,
        0.17914725904693657,
        0.2684563758389262,
        0.2579535683576956,
        0.23629489603024575,
        0.1012829169480081,
        0.1601024655779699
      ],
      "backgroundColor": "rgba(0, 85, 255, 0.4)",
      "borderColor": "rgba(0, 85, 255, 1)",
      "borderWidth": 2
    }
  ]
}
```

### Income vs education

```json
{
  "type": "scatter",
  "label": "Average income vs education",
  "labels": [],
  "datasets": [
    {
      "label": "Average income",
      "data": [
        {
          "x": 9.5,
          "y": 187383.17757009345
        },
        {
          "x": 12.5,
          "y": 162429.6435272045
        },
        {
          "x": 13,
          "y": 213339.89316840033
        },
        {
          "x": 14.5,
          "y": 229898.64864864864
        },
        {
          "x": 17,
          "y": 247647.0588235294
        },
        {
          "x": 19,
          "y": 313552.83307810104
        }
      ],
      "backgroundColor": "rgba(255, 170, 0, 0.7)",
      "borderColor": "rgba(255, 170, 0, 1)",
      "borderWidth": 2
    }
  ]
}
```
