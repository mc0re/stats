﻿using System.Collections.Concurrent;
using System.IO;


namespace TextData
{
    /// <summary>
    /// Full word lists: 
    /// http://www.ashley-bovan.co.uk/words/partsofspeech.html
    /// </summary>

    public class WordClass
    {
        #region Fields

        private ConcurrentDictionary<string, WordClasses> mWords = new ConcurrentDictionary<string, WordClasses>();

        #endregion


        #region Init and clean-up

        public WordClass()
        {
            foreach (var nounFile in Directory.EnumerateFiles("Data/nouns"))
            {
                foreach (var word in File.ReadAllLines(nounFile))
                {
                    if (!string.IsNullOrWhiteSpace(word))
                        mWords.AddOrUpdate(word, WordClasses.Noun, (_, cl) => cl | WordClasses.Noun);
                }
            }

            foreach (var nounFile in Directory.EnumerateFiles("Data/verbs"))
            {
                foreach (var word in File.ReadAllLines(nounFile))
                {
                    if (!string.IsNullOrWhiteSpace(word))
                        mWords.AddOrUpdate(word, WordClasses.Verb, (_, cl) => cl | WordClasses.Verb);
                }
            }

            foreach (var nounFile in Directory.EnumerateFiles("Data/adjectives"))
            {
                foreach (var word in File.ReadAllLines(nounFile))
                {
                    if (!string.IsNullOrWhiteSpace(word))
                        mWords.AddOrUpdate(word, WordClasses.Adjective, (_, cl) => cl | WordClasses.Adjective);
                }
            }

            foreach (var nounFile in Directory.EnumerateFiles("Data/adverbs"))
            {
                foreach (var word in File.ReadAllLines(nounFile))
                {
                    if (!string.IsNullOrWhiteSpace(word))
                        mWords.AddOrUpdate(word, WordClasses.AdVerb, (_, cl) => cl | WordClasses.AdVerb);
                }
            }
        }

        #endregion


        #region API

        public WordClasses GetWordClass(string word)
        {
            if (mWords.TryGetValue(word, out var cl))
                return cl;

            return WordClasses.Unknown;
        }

        #endregion
    }
}
