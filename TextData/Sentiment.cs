﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace TextData
{
    /// <summary>
    /// Sentiment-based word lists (positive or negative words):
    /// https://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html#lexicon
    /// http://mpqa.cs.pitt.edu/#subj_lexicon 
    /// </summary>
    public class Sentiment
    {
        #region Fields

        private readonly Dictionary<string, Polarities> mWordList = new Dictionary<string, Polarities>();

        #endregion


        #region Init and clean-up

        public Sentiment()
        {
            ReadPositive(mWordList);
            ReadNegative(mWordList);
            ReadSentiments(mWordList);
        }

        #endregion


        #region API

        public Polarities GetPolarity(string word)
        {
            if (!mWordList.TryGetValue(word, out var polarity))
                return Polarities.Unknown;

            return polarity;
        }

        #endregion


        #region Utility

        /// <summary>
        /// Read and collect a list of positive words.
        /// </summary>
        private void ReadPositive(Dictionary<string, Polarities> words)
        {
            foreach (var word in ReadAndSkipComments("sentiment/positive-words.txt"))
            {
                words.TryAdd(word, Polarities.Positive);
            }
        }


        /// <summary>
        /// Read and collect a list of negative words.
        /// </summary>
        private void ReadNegative(Dictionary<string, Polarities> words)
        {
            foreach (var word in ReadAndSkipComments("sentiment/negative-words.txt"))
            {
                words.TryAdd(word, Polarities.Negative);
            }
        }


        /// <summary>
        /// Read the sentiments file.
        /// </summary>
        private void ReadSentiments(Dictionary<string, Polarities> words)
        {
            foreach (var line in File.ReadAllLines("Data/subjectivity/subjclueslen1.tff"))
            {
                if (string.IsNullOrWhiteSpace(line))
                    continue;

                var fields = line.Split(" ");
                string word = null;
                var sentiment = Polarities.Positive;
                var skipWord = false;

                foreach (var field in fields)
                {
                    var parts = field.Split("=");
                    switch (parts[0])
                    {
                        case "len":
                            if (parts[1] != "1")
                                throw new ArgumentException($"Expecting length 1, got {parts[1]}.");
                            break;

                        case "word1":
                            word = parts[1];
                            break;

                        case "priorpolarity":
                            switch (parts[1])
                            {
                                case "negative":
                                case "weakneg":
                                    sentiment = Polarities.Negative; break;
                                case "positive":
                                    sentiment = Polarities.Positive; break;
                                case "neutral":
                                case "both":
                                    skipWord = true; break;
                                default:
                                    throw new ArgumentException($"Unrecognized sentiment '{parts[1]}'.");
                            };
                            break;
                    }
                }

                if (skipWord) continue;
                if (word is null)
                    throw new ArgumentException("Word not defined.");

                words.TryAdd(word, sentiment);
            }
        }


        /// <summary>
        /// Read file contents, skip empty lines and comments (starting with semicolon).
        /// </summary>
        private static IEnumerable<string> ReadAndSkipComments(string fileName)
        {
            return from line in File.ReadAllLines($"Data/{fileName}")
                   where !string.IsNullOrWhiteSpace(line) &&
                         line[0] != ';'
                   select line;
        }

        #endregion
    }
}
