﻿using System;


namespace TextData
{
    [Flags]
    public enum WordClasses
    {
        Unknown = 0,
        Noun = 1,
        Verb = 2,
        Adjective = 4,
        AdVerb = 8
    }
}
