﻿namespace TextData
{
    public enum Polarities
    {
        Unknown,
        Positive,
        Negative
    }
}