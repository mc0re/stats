﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;


namespace StatsData
{
    /// <summary>
    /// Input data repository. Loaded from a file and parsed into <see cref="DataRow"/> entities.
    /// </summary>
    public class DataRepository
    {
        #region API

        /// <summary>
        /// Read the data from a file and parse into a list of <see cref="DataRow"/>.
        /// </summary>
        /// <returns></returns>
        public ICollection<DataRow> Get()
        {
            var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                "Data",
                "MandagMorgen-samlet_F1.csv");

            if (!File.Exists(path))
                throw new ArgumentException($"Data file not found in '{path}'. Is its Copy action set?");

            var lines = File.ReadLines(path);
            var res = new List<DataRow>();

            foreach (var line in lines.Skip(1))
            {
                var row = ParseRow(line);
                if (row is null)
                    continue;

                res.Add(row);
            }

            return res;
        }

        #endregion


        #region Utility

        private static DataRow ParseRow(string line)
        {
            var fields = line.Split(',');

            var row = new DataRow
            {
                Id = int.Parse(fields[0]),
                Gender = fields[1] switch { "1" => Genders.Male, "2" => Genders.Female, _ => Genders.Unspecified },
                BirthYear = int.TryParse(fields[2], out var byr) ? (int?)byr + 1900 : null,
                Age = int.TryParse(fields[3], out var age) ? (int?)age : null,
                AgeGroup = int.TryParse(fields[4], out var agr) ? GetAgeGroup(agr) : AgeGroups.Unspecified,
                School = int.TryParse(fields[5], out var sgr) ? GetSchoolGroup(sgr) : SchoolGroups.Unspecified,
                Education = int.TryParse(fields[6], out var egr) ? GetEducationGroup(egr) : EducationGroups.Unspecified,
                Vocational = int.TryParse(fields[7], out var vgr) ? GetVocationalGroup(vgr) : VocationalGroups.Unspecified,
                JobStatus = int.TryParse(fields[8], out var jgr) ? GetJobStatus(jgr) : JobStatuses.Unspecified,
                LeadershipStatus = int.TryParse(fields[9], out var lgr) ? GetLeadershipStatus(lgr) : LeadershipStatuses.Unspecified,
                HousingStatus = int.TryParse(fields[10], out var hgr) ? GetHousingStatus(hgr) : HousingStatuses.Unspecified,
                HousingType = int.TryParse(fields[11], out var htp) ? GetHousingType(htp) : HousingTypes.Unspecified,
                Occupation = int.TryParse(fields[14], out var oc3) ? GetOccupation(oc3) : Occupations.Unspecified,
                OccupationGroup = int.TryParse(fields[15], out var ogr) ? GetOccupationGroup(ogr) : OccupationGroups.Unspecified,
                Location = int.TryParse(fields[16], out var loc) ? GetLocation(loc) : Locations.Unspecified,
                HouseholdIncome = int.TryParse(fields[19], out var hi3) ? GetIncome(hi3) : Incomes.Unspecified,
                OwnIncome = int.TryParse(fields[20], out var oi) ? GetIncomeV2(oi) : Incomes.Unspecified,
                LocationAmt = int.TryParse(fields[21], out var amt) ? GetLocationAmt(amt) : LocationAmts.Unspecified,
                CivilStatus = int.TryParse(fields[22], out var civ) ? GetCivilStatus(civ) : CivilStatuses.Unspecified,
                Batch = int.TryParse(fields[42], out var bat) ? bat : 0,
                Year = int.TryParse(fields[43], out var qyr) ? qyr : 0,
                Month = int.TryParse(fields[44], out var qmn) ? (int?)qmn : null,
                Day = int.TryParse(fields[45], out var qdy) ? (int?)qdy : null
            };

            row.MinHouseholdIncome = GetRangeMin(row.HouseholdIncome);
            row.MaxHouseholdIncome = GetRangeMax(row.HouseholdIncome);
            row.MinOwnIncome = GetRangeMin(row.OwnIncome);
            row.MaxOwnIncome = GetRangeMax(row.OwnIncome);
            row.MinEducation = GetRangeMin(row.Education);
            row.MaxEducation = GetRangeMax(row.Education);

            return row;
        }


        private static AgeGroups GetAgeGroup(int groupIdx) => groupIdx switch
        {
            1 => AgeGroups.Teen,
            2 => AgeGroups.Lower20,
            3 => AgeGroups.Upper20,
            4 => AgeGroups.Lower30,
            5 => AgeGroups.Upper30,
            6 => AgeGroups.Lower40,
            7 => AgeGroups.Upper40,
            8 => AgeGroups.Lower50,
            9 => AgeGroups.Upper50,
            10 => AgeGroups.Lower60,
            11 => AgeGroups.Upper60,
            12 => AgeGroups.Lower70,
            13 => AgeGroups.Senior,
            _ => AgeGroups.Unspecified
        };


        private static SchoolGroups GetSchoolGroup(int groupIdx) => groupIdx switch
        {
            1 => SchoolGroups.Low,
            2 => SchoolGroups.Medium,
            3 => SchoolGroups.Full,
            4 => SchoolGroups.Gymnasium,
            8 => SchoolGroups.NotAnswered,
            9 => SchoolGroups.DoNotKnow,
            _ => SchoolGroups.Unspecified
        };


        private static EducationGroups GetEducationGroup(int groupIdx) => groupIdx switch
        {
            1 => EducationGroups.LowSchool,
            2 => EducationGroups.FullSchool,
            3 => EducationGroups.Gymnasium,
            4 => EducationGroups.Vocational,
            5 => EducationGroups.HighShort,
            6 => EducationGroups.HighMedium,
            7 => EducationGroups.HighLong,
            8 => EducationGroups.Other,
            9 => EducationGroups.None,
            _ => EducationGroups.Unspecified
        };


        private static VocationalGroups GetVocationalGroup(int groupIdx) => groupIdx switch
        {
            1 => VocationalGroups.Basic,
            2 => VocationalGroups.Complete,
            3 => VocationalGroups.HigherShort,
            4 => VocationalGroups.HigherMedium,
            5 => VocationalGroups.HigherLong,
            6 => VocationalGroups.Other,
            7 => VocationalGroups.None,
            8 => VocationalGroups.NotAnswered,
            9 => VocationalGroups.DoNotKnow,
            _ => VocationalGroups.Unspecified
        };


        private static JobStatuses GetJobStatus(int statusIdx) => statusIdx switch
        {
            1 => JobStatuses.Unemployed,
            2 => JobStatuses.MaternalLeave,
            3 => JobStatuses.Other,
            8 => JobStatuses.NotAnswered,
            9 => JobStatuses.DoNotKnow,
            _ => JobStatuses.Unspecified
        };


        private static LeadershipStatuses GetLeadershipStatus(int statusIdx) => statusIdx switch
        {
            1 => LeadershipStatuses.Leader,
            2 => LeadershipStatuses.NotLeader,
            8 => LeadershipStatuses.NotAnswered,
            9 => LeadershipStatuses.DoNotKnow,
            _ => LeadershipStatuses.Unspecified
        };


        private static HousingStatuses GetHousingStatus(int statusIdx) => statusIdx switch
        {
            1 => HousingStatuses.Rent,
            2 => HousingStatuses.Partial,
            3 => HousingStatuses.Own,
            4 => HousingStatuses.Other,
            8 => HousingStatuses.NotAnswered,
            9 => HousingStatuses.DoNotKnow,
            _ => HousingStatuses.Unspecified
        };


        private static HousingTypes GetHousingType(int typeIdx) => typeIdx switch
        {
            1 => HousingTypes.House,
            2 => HousingTypes.Farm,
            3 => HousingTypes.Appartment,
            4 => HousingTypes.Room,
            5 => HousingTypes.NotAnswered,
            6 => HousingTypes.DoNotKnow,
            _ => HousingTypes.Unspecified
        };


        private static Occupations GetOccupation(int groupIdx) => groupIdx switch
        {
            1 => Occupations.UneducatedWorker,
            2 => Occupations.EducatedWorker,
            3 => Occupations.LowEmployee,
            4 => Occupations.HighEmployee,
            5 => Occupations.SelfEmployed,
            6 => Occupations.Student,
            7 => Occupations.NotOnMarket,
            8 => Occupations.HomegoingSpouse,
            9 => Occupations.HelpingSpouse,
            _ => Occupations.Unspecified
        };


        private static OccupationGroups GetOccupationGroup(int groupIdx) => groupIdx switch
        {
            1 => OccupationGroups.Private,
            2 => OccupationGroups.Public,
            3 => OccupationGroups.SelfEmployed,
            4 => OccupationGroups.Unemployed,
            7 => OccupationGroups.WasNotAsked,
            8 => OccupationGroups.NotAnswered,
            9 => OccupationGroups.DoNotKnow,
            _ => OccupationGroups.Unspecified
        };


        private static Locations GetLocation(int groupIdx) => groupIdx switch
        {
            1 => Locations.CapitalArea,
            2 => Locations.Islands,
            3 => Locations.Jutland,
            _ => Locations.Unspecified
        };


        private static Incomes GetIncome(int groupIdx) => groupIdx switch
        {
            1 => Incomes.Under20K_V1,
            2 => Incomes.Under70K_V1,
            3 => Incomes.Under100K_V1,
            4 => Incomes.Under100K_V2,
            5 => Incomes.Under120K_V1,
            6 => Incomes.Under150K_V1,
            7 => Incomes.Under150K_V2,
            8 => Incomes.Under200K,
            9 => Incomes.Under250K,
            10 => Incomes.Under300K,
            11 => Incomes.Under400K,
            12 => Incomes.Under500K,
            13 => Incomes.Under600K,
            14 => Incomes.Under700K_V2,
            15 => Incomes.Over600K_V1,
            16 => Incomes.Under800K_V2,
            17 => Incomes.Under900KV2,
            18 => Incomes.Over900KV2,
            98 => Incomes.NotAnswered,
            99 => Incomes.DoNotKnow,
            _ => Incomes.Unspecified
        };


        private static Incomes GetIncomeV2(int groupIdx) => groupIdx switch
        {
            1 => Incomes.Under100K_V2,
            2 => Incomes.Under150K_V2,
            3 => Incomes.Under200K,
            4 => Incomes.Under250K,
            5 => Incomes.Under300K,
            6 => Incomes.Under400K,
            7 => Incomes.Under500K,
            8 => Incomes.Under600K,
            9 => Incomes.Under700K_V2,
            10 => Incomes.Under800K_V2,
            11 => Incomes.Under900KV2,
            12 => Incomes.Over900KV2,
            98 => Incomes.NotAnswered,
            99 => Incomes.DoNotKnow,
            _ => Incomes.Unspecified
        };


        private static LocationAmts GetLocationAmt(int amtIdx) => amtIdx switch
        {
            1 => LocationAmts.KobenhavnFrederiksberg,
            2 => LocationAmts.Kobenhavn,
            3 => LocationAmts.Frederiksborg,
            4 => LocationAmts.Roskilde,
            5 => LocationAmts.Vestsjaelland,
            6 => LocationAmts.Storstroem,
            7 => LocationAmts.Bornholm,
            8 => LocationAmts.Fyn,
            9 => LocationAmts.Soenderjylland,
            10 => LocationAmts.Ribe,
            11 => LocationAmts.Vejle,
            12 => LocationAmts.Rinkoebing,
            13 => LocationAmts.Aarhus,
            14 => LocationAmts.Viborg,
            15 => LocationAmts.Nordjylland,
            _ => LocationAmts.Unspecified
        };


        private static CivilStatuses GetCivilStatus(int statusIndex) => statusIndex switch
        {
            1 => CivilStatuses.NeverMarried,
            2 => CivilStatuses.Married,
            3 => CivilStatuses.PreviouslyMarried,
            8 => CivilStatuses.NotAnswered,
            9 => CivilStatuses.DoNotKnow,
            _ => CivilStatuses.Unspecified
        };


        private static double GetRangeMin<T>(T value) where T : struct
        {
            var fi = typeof(T).GetField(value.ToString());
            return fi.GetCustomAttribute<RangeMinAttribute>()?.MinValue ?? 0;
        }


        private static double GetRangeMax<T>(T value) where T : struct
        {
            var fi = typeof(T).GetField(value.ToString());
            return fi.GetCustomAttribute<RangeMaxAttribute>()?.MaxValue ?? 0;
        }

        #endregion
    }
}
