﻿namespace StatsData
{
    /// <summary>
    /// This enum contains V1 and V2 values. For some properties they are used
    /// exclusively (V1 or V2), for some - all are used intermixed.
    /// </summary>
    public enum Incomes
    {
        /// <summary>
        /// Unspecified or invalid value.
        /// </summary>
        Unspecified,

        /// <summary>
        /// 0-19,999
        /// </summary>
        [RangeMin(0)]
        [RangeMax(20_000)]
        Under20K_V1,

        /// <summary>
        /// 20,000-69,999
        /// </summary>
        [RangeMin(20_000)]
        [RangeMax(70_000)]
        Under70K_V1,

        /// <summary>
        /// 70,000-99,999
        /// </summary>
        [RangeMin(70_000)]
        [RangeMax(100_000)]
        Under100K_V1,

        /// <summary>
        /// 0-99,999
        /// </summary>
        [RangeMin(0)]
        [RangeMax(100_000)]
        Under100K_V2,

        /// <summary>
        /// 100,000-119,999
        /// </summary>
        [RangeMin(100_000)]
        [RangeMax(120_000)]
        Under120K_V1,

        /// <summary>
        /// 120,000-149,999
        /// </summary>
        [RangeMin(120_000)]
        [RangeMax(150_000)]
        Under150K_V1,

        /// <summary>
        /// 100,000-149,999
        /// </summary>
        [RangeMin(100_000)]
        [RangeMax(150_000)]
        Under150K_V2,

        [RangeMin(150_000)]
        [RangeMax(200_000)]
        Under200K,

        [RangeMin(200_000)]
        [RangeMax(250_000)]
        Under250K,

        [RangeMin(250_000)]
        [RangeMax(300_000)]
        Under300K,

        [RangeMin(300_000)]
        [RangeMax(400_000)]
        Under400K,

        [RangeMin(400_000)]
        [RangeMax(500_000)]
        Under500K,

        [RangeMin(500_000)]
        [RangeMax(600_000)]
        Under600K,

        /// <summary>
        /// 600,000-?
        /// </summary>
        [RangeMin(300_000)]
        [RangeMax(double.PositiveInfinity)]
        Over600K_V1,

        /// <summary>
        /// 600,000-699,999
        /// </summary>
        [RangeMin(600_000)]
        [RangeMax(700_000)]
        Under700K_V2,

        [RangeMin(700_000)]
        [RangeMax(800_000)]
        Under800K_V2,

        [RangeMin(800_000)]
        [RangeMax(900_000)]
        Under900KV2,

        /// <summary>
        /// 900,000-?
        /// </summary>
        [RangeMin(900_000)]
        [RangeMax(double.PositiveInfinity)]
        Over900KV2,

        NotAnswered,

        DoNotKnow
    }
}