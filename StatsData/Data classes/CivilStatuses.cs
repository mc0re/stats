﻿namespace StatsData
{
    public enum CivilStatuses
    {
        /// <summary>
        /// Unspecified or invalid value.
        /// </summary>
        Unspecified,

        NeverMarried,

        /// <summary>
        /// Gift / samboende.
        /// </summary>
        Married,

        /// <summary>
        /// Skilt / separeret / enkemand / enke / tidligere samlevende.
        /// </summary>
        PreviouslyMarried,

        NotAnswered,

        DoNotKnow
    }
}