﻿namespace StatsData
{
    public enum OccupationGroups
    {
        /// <summary>
        /// Unspecified or not allowed value.
        /// </summary>
        Unspecified,

        Private,

        Public,

        SelfEmployed,

        Unemployed,

        WasNotAsked,

        NotAnswered,

        DoNotKnow
    }
}