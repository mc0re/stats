﻿using System;


namespace StatsData
{
    /// <summary>
    /// The lower bound of a range.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class RangeMinAttribute : Attribute
    {
        /// <summary>
        /// The lower bound of the range (inluded).
        /// </summary>
        public double MinValue { get; }


        public RangeMinAttribute(double minValue)
        {
            MinValue = minValue;
        }
    }
}
