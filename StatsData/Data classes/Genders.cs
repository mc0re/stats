﻿namespace StatsData
{
    public enum Genders
    {
        Unspecified,
        Female,
        Male
    }
}