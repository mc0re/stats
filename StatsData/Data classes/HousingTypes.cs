﻿namespace StatsData
{
    public enum HousingTypes
    {
        /// <summary>
        /// Unspecified or not allowed value.
        /// </summary>
        Unspecified,

        /// <summary>
        /// Villa, townhouse.
        /// </summary>
        House,

        Farm,

        Appartment,

        Room,

        NotAnswered,

        DoNotKnow
    }
}