﻿namespace StatsData
{
    public enum VocationalGroups
    {
        /// <summary>
        /// Unspecified or not a valid value.
        /// </summary>
        Unspecified,

        None,


        /// <summary>
        /// Only basis education.
        /// </summary>
        Basic,


        /// <summary>
        /// Completed the education.
        /// </summary>
        Complete,


        /// <summary>
        /// Took on a short higher education.
        /// </summary>
        HigherShort,


        /// <summary>
        /// Took on a medium higher education.
        /// </summary>
        HigherMedium,


        /// <summary>
        /// Took on a long higher education.
        /// </summary>
        HigherLong,

        Other,

        NotAnswered,

        DoNotKnow
    }
}