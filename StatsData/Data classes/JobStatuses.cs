﻿namespace StatsData
{
    /// <summary>
    /// LImited information about job status.
    /// </summary>
    public enum JobStatuses
    {
        /// <summary>
        /// Unspecified or nto a valid value.
        /// </summary>
        Unspecified,

        Unemployed,

        MaternalLeave,

        Other,

        NotAnswered,

        DoNotKnow
    }
}