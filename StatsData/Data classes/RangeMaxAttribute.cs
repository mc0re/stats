﻿using System;

namespace StatsData
{
    /// <summary>
    /// The upper bound of a range.
    /// </summary>
    public class RangeMaxAttribute : Attribute
    {
        /// <summary>
        /// The upper bound of the range (excluded).
        /// Can be <see cref="double.PositiveInfinity"/>.
        /// </summary>
        public double MaxValue { get; }


        public RangeMaxAttribute(double maxValue)
        {
            MaxValue = maxValue;
        }
    }
}