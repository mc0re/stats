﻿namespace StatsData
{
    public enum Occupations
    {
        /// <summary>
        /// Unspecified or not allowed value.
        /// </summary>
        Unspecified,

        /// <summary>
        /// Ufaglært arbejder/specialarbejder.
        /// </summary>
        UneducatedWorker,

        /// <summary>
        /// Arbejder, faglært.
        /// </summary>
        EducatedWorker,

        /// <summary>
        /// Laverere funktionær / tjenestemand.
        /// </summary>
        LowEmployee,

        /// <summary>
        /// Højere funktionær / tjenestand.
        /// </summary>
        HighEmployee,

        /// <summary>
        /// Selvstændig, f.eks. landbruger, detailhandel / håndværksmester.
        /// </summary>
        SelfEmployed,

        /// <summary>
        /// Lærling / elev / studerende etc.
        /// </summary>
        Student,

        /// <summary>
        /// Ude af erhverv (pensionist, efterlønsmodtager, uarbejdsdygtig).
        /// </summary>
        NotOnMarket,

        /// <summary>
        /// Gift kvinde uden selverhverv.
        /// </summary>
        HomegoingSpouse,

        /// <summary>
        /// Medhjælpende ægtefælle.
        /// </summary>
        HelpingSpouse,

        NotAnswered,

        DoNotKnow
    }
}