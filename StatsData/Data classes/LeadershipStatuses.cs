﻿namespace StatsData
{
    /// <summary>
    /// Whether the person has a management role.
    /// </summary>
    public enum LeadershipStatuses
    {
        /// <summary>
        /// Unspecified or not allowed value.
        /// </summary>
        Unspecified,


        /// <summary>
        /// Has responsibility over 3 or more persons.
        /// </summary>
        Leader,


        NotLeader,

        NotAnswered,

        DoNotKnow
    }
}