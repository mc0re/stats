﻿namespace StatsData
{
    public class DataRow
    {
        public int Id { get; internal set; }


        #region Personal

        public Genders Gender { get; internal set; }

        public int? BirthYear { get; internal set; }

        public int? Age { get; internal set; }

        public AgeGroups AgeGroup { get; internal set; }

        public CivilStatuses CivilStatus { get; internal set; }

        #endregion


        #region Education

        public SchoolGroups School { get; internal set; }

        public EducationGroups Education { get; internal set; }

        public VocationalGroups Vocational { get; internal set; }

        public double MinEducation { get; internal set; }

        public double MaxEducation { get; internal set; }

        #endregion


        #region Job

        public JobStatuses JobStatus { get; internal set; }

        public LeadershipStatuses LeadershipStatus { get; internal set; }

        public Occupations Occupation { get; internal set; }

        public OccupationGroups OccupationGroup { get; internal set; }

        #endregion


        #region Housing and location

        public HousingStatuses HousingStatus { get; internal set; }

        public HousingTypes HousingType { get; internal set; }

        public Locations Location { get; internal set; }

        public LocationAmts LocationAmt { get; internal set; }

        #endregion


        #region Income

        /// <summary>
        /// V1 and V2 merged.
        /// </summary>
        public Incomes HouseholdIncome { get; internal set; }

        public double MinHouseholdIncome { get; internal set; }

        public double MaxHouseholdIncome { get; internal set; }

        /// <summary>
        /// V2 only.
        /// </summary>
        public Incomes OwnIncome { get; internal set; }

        public double MinOwnIncome { get; internal set; }

        public double MaxOwnIncome { get; internal set; }

        #endregion


        #region Schema info

        /// <summary>
        /// Questioning batch (1..).
        /// </summary>
        public int Batch { get; internal set; }

        /// <summary>
        /// Date for the quesionairy.
        /// </summary>
        public int Year { get; internal set; }

        /// <summary>
        /// Date for the quesionairy.
        /// </summary>
        public int? Month { get; internal set; }

        /// <summary>
        /// Date for the quesionairy.
        /// </summary>
        public int? Day { get; internal set; }

        #endregion
    }
}
