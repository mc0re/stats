﻿namespace StatsData
{
    /// <summary>
    /// Age buckets.
    /// </summary>
    public enum AgeGroups
    {
        /// <summary>
        /// Unspecified or beyond the limits.
        /// </summary>
        Unspecified,


        /// <summary>
        /// 18-19.
        /// </summary>
        Teen,


        /// <summary>
        /// 20-24.
        /// </summary>
        Lower20,


        /// <summary>
        /// 25-29.
        /// </summary>
        Upper20,


        /// <summary>
        /// 30-34.
        /// </summary>
        Lower30,


        /// <summary>
        /// 35-39.
        /// </summary>
        Upper30,


        /// <summary>
        /// 40-44.
        /// </summary>
        Lower40,


        /// <summary>
        /// 45-49.
        /// </summary>
        Upper40,


        /// <summary>
        /// 50-54.
        /// </summary>
        Lower50,


        /// <summary>
        /// 55-59.
        /// </summary>
        Upper50,


        /// <summary>
        /// 60-64.
        /// </summary>
        Lower60,


        /// <summary>
        /// 65-69.
        /// </summary>
        Upper60,


        /// <summary>
        /// 70-74.
        /// </summary>
        Lower70,


        /// <summary>
        /// 75+
        /// </summary>
        Senior
    }
}