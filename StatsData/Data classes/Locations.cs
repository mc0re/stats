﻿namespace StatsData
{
    public enum Locations
    {
        /// <summary>
        /// Unspecified or invalid value.
        /// </summary>
        Unspecified,

        CapitalArea,

        Islands,

        Jutland
    }
}