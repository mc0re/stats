﻿namespace StatsData
{
    /// <summary>
    /// Highest achieved scholar or gymnasial education.
    /// </summary>
    public enum SchoolGroups
    {
        /// <summary>
        /// Unspecified or beyond the limits.
        /// </summary>
        Unspecified,


        /// <summary>
        /// 7 years or less.
        /// </summary>
        Low,


        /// <summary>
        /// 8-9 years.
        /// </summary>
        Medium,


        /// <summary>
        /// 10 years, school exam
        /// </summary>
        Full,


        /// <summary>
        /// Gymnasium exam or HF.
        /// </summary>
        Gymnasium,


        /// <summary>
        /// Preferred not to answer.
        /// </summary>
        NotAnswered,


        /// <summary>
        /// Another possible negative answer.
        /// </summary>
        DoNotKnow
    }
}