﻿namespace StatsData
{
    /// <summary>
    /// Highest achieve education.
    /// </summary>
    public enum EducationGroups
    {
        /// <summary>
        /// Unspecified or beyond the allowed values.
        /// </summary>
        Unspecified,


        None,


        /// <summary>
        /// School, 8 years or less.
        /// </summary>
        [RangeMin(0)]
        [RangeMax(8)]
        LowSchool,


        /// <summary>
        /// 9 or 10 years, school examen.
        /// </summary>
        [RangeMin(9)]
        [RangeMax(10)]
        FullSchool,


        /// <summary>
        /// HF, HTX, HHX - 12 years.
        /// </summary>
        [RangeMin(12)]
        [RangeMax(13)]
        Gymnasium,


        /// <summary>
        /// Vocational education.
        /// </summary>
        [RangeMin(12)]
        [RangeMax(14)]
        Vocational,


        /// <summary>
        /// 3 years or less of higher education.
        /// </summary>
        [RangeMin(13)]
        [RangeMax(16)]
        HighShort,


        /// <summary>
        /// 4-5 years of higher education.
        /// </summary>
        [RangeMin(16)]
        [RangeMax(18)]
        HighMedium,


        /// <summary>
        /// 5 or more years of higher education.
        /// </summary>
        [RangeMin(17)]
        [RangeMax(21)]
        HighLong,


        Other
    }
}