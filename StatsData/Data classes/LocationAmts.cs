﻿namespace StatsData
{
    public enum LocationAmts
    {
        /// <summary>
        /// Unpecified or invalid value.
        /// </summary>
        Unspecified,

        /// <summary>
        /// Københavns/Frederiksberg kommune.
        /// </summary>
        KobenhavnFrederiksberg,

        Kobenhavn,

        Frederiksborg,

        Roskilde,

        Vestsjaelland,

        Storstroem,

        Bornholm,

        Fyn,

        Soenderjylland,

        Ribe,

        Vejle,

        Rinkoebing,

        Aarhus,

        Viborg,

        Nordjylland
    }
}