﻿namespace StatsData
{
    public enum HousingStatuses
    {
        /// <summary>
        /// Unspecified or not a valid value.
        /// </summary>
        Unspecified,

        /// <summary>
        /// Leje / fremleje.
        /// </summary>
        Rent,

        /// <summary>
        /// Andelsbolig.
        /// </summary>
        Partial,

        Own,

        Other,

        NotAnswered,

        DoNotKnow
    }
}