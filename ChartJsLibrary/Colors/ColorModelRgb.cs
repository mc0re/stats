﻿using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace ChartJsLibrary
{
    public class ColorModelRgb
    {
        #region Properties

        public double Alpha { get; internal set; }

        public byte Red { get; internal set; }

        public byte Green { get; internal set; }

        public byte Blue { get; internal set; }

        #endregion


        #region API

        public ColorModelRgb SetAlpha(double alpha) =>
            new ColorModelRgb { Alpha = alpha, Red = Red, Green = Green, Blue = Blue };


        public string ToJavaScript() => $"rgba({Red}, {Green}, {Blue}, {Alpha.ToString(CultureInfo.InvariantCulture)})";

        #endregion


        #region ToString

        /// <summary>
        /// Debugging.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public override string ToString()
        {
            return $"R={Red}, G={Green}, B={Blue}, A={Alpha}";
        }

        #endregion
    }
}