﻿using System.Collections.Generic;


namespace ChartJsLibrary
{
    /// <summary>
    /// Generate the given number of distinct colours.
    /// </summary>
    public static class ColorGenerator
    {
        /// <summary>
        /// Generate the given number of distinct colours.
        /// </summary>
        public static IList<ColorModelRgb> Create(int count, double alpha, int startAngle)
        {
            var res = new List<ColorModelRgb>();

            for (var colorIdx = 0; colorIdx < count; colorIdx++)
            {
                // Hue is 0..359
                var hue = 360.0 * colorIdx / count + startAngle;
                while (hue >= 360) hue -= 360;
                // Lightness 0.5 is in the middle between black and white
                var hsl = new ColorModelHsl { Hue = hue, Saturation = 1, Lightness = 0.5 };
                // Add the RGB colour with the requested alpha
                res.Add(hsl.ToRgb(alpha));
            }

            return res;
        }
    }
}