﻿using System;
using System.Diagnostics.CodeAnalysis;


namespace ChartJsLibrary
{
    public struct ColorModelHsl
    {
        #region Properties

        /// <summary>
        /// In degrees, 0..359.
        /// </summary>
        public double Hue { get; set; }

        /// <summary>
        /// 0..1.
        /// </summary>
        public double Saturation { get; set; }

        /// <summary>
        /// 0..1.
        /// </summary>
        public double Lightness { get; set; }

        #endregion


        #region Equality

        public override int GetHashCode()
        {
            return Hue.GetHashCode() ^ Saturation.GetHashCode() ^ Lightness.GetHashCode();
        }


        public override bool Equals(object obj)
        {
            var other = (ColorModelHsl)obj;

            return Utility.AlmostEqual(Hue, other.Hue) &&
                   Utility.AlmostEqual(Saturation, other.Saturation) &&
                   Utility.AlmostEqual(Lightness, other.Lightness);
        }


        public static bool operator ==(ColorModelHsl value1, ColorModelHsl value2)
        {
            return value1.Equals(value2);
        }


        public static bool operator !=(ColorModelHsl value1, ColorModelHsl value2)
        {
            return !value1.Equals(value2);
        }

        #endregion


        #region Conversion

        /// <summary>
        /// Create an HSL structure from RGB.
        /// </summary>
        public static ColorModelHsl FromRgb(byte red, byte green, byte blue)
        {
            var res = new ColorModelHsl();
            var max = Math.Max(Math.Max(red, green), blue);
            var min = Math.Min(Math.Min(red, green), blue);

            var chroma = max - min;
            res.Lightness = (max + min) / 510.0;  // Maximum 1.0

            if (chroma != 0)
            {
                res.Saturation = chroma / (1 - Math.Abs(2 * res.Lightness - 1)) / 255;
                var q = 60.0 / chroma;

                if (max == red)
                {
                    if (blue > green)
                        res.Hue = q * (green - blue) + 360;
                    else
                        res.Hue = q * (green - blue);
                }
                else if (max == green)
                {
                    res.Hue = q * (blue - red) + 120;
                }
                else //if (max == blue)
                {
                    res.Hue = q * (red - green) + 240;
                }
            }

            return res;
        }


        /// <summary>
        /// Convert HSL to RGB with the given Alpha.
        /// </summary>
        /// <param name="alpha">0..1</param>
        public ColorModelRgb ToRgb(double alpha)
        {
            double red;
            double green;
            double blue;
            var chroma = Saturation * (1 - Math.Abs(2 * Lightness - 1));
            var x = chroma * (1 - Math.Abs(Hue / 60 % 2 - 1));

            if (0 <= Hue && Hue <= 60)
            {
                red = chroma; green = x; blue = 0;
            }

            else if (60 <= Hue && Hue <= 120)
            {
                red = x; green = chroma; blue = 0;
            }

            else if (120 <= Hue && Hue <= 180)
            {
                red = 0; green = chroma; blue = x;
            }

            else if (180 <= Hue && Hue <= 240)
            {
                red = 0; green = x; blue = chroma;
            }

            else if (240 <= Hue && Hue <= 300)
            {
                red = x; green = 0; blue = chroma;
            }

            else //if (300 <= Hue && Hue <= 360)
            {
                red = chroma; green = 0; blue = x;
            }

            var add = Lightness - chroma / 2;

            return new ColorModelRgb
            {
                Alpha = alpha,
                Red = (byte)((red + add) * 255),
                Green = (byte)((green + add) * 255),
                Blue = (byte)((blue + add) * 255)
            };
        }

        #endregion


        #region ToString

        /// <summary>
        /// Debugging.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public override string ToString()
        {
            return $"H={Hue}, S={Saturation}, L={Lightness}";
        }

        #endregion
    }
}
