﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;


namespace ChartJsLibrary
{
    /// <summary>
    /// Create JSON data for Chart.js.
    /// </summary>
    public static class ChartJs
    {
        #region API

        public static string Serialize(ChartDataModel model)
        {
            // If not set yet
            model.SetDefaultColors();

            var dataDto = new ChartDataDto
            {
                Type = model.Type.ToString().ToCamelCase(),
                Label = model.Label
            };

            switch (model.Type)
            {
                case ChartTypes.Scatter:
                    AddXyData(dataDto, model);
                    break;

                case ChartTypes.Bubble:
                    AddXyzData(dataDto, model);
                    break;

                default:
                    AddCategorizedData(dataDto, model);
                    break;
            }

            var opt = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };
            var res = JsonSerializer.Serialize<object>(dataDto, opt);

            return res;
        }

        #endregion


        #region Utility

        private static void AddCategorizedData(ChartDataDto dataDto, ChartDataModel model)
        {
            var firstRow = true;

            foreach (var setModel in model.DataSets)
            {
                var setDto = CreateDatasetDto(setModel);
                dataDto.Datasets.Add(setDto);
                var dataList = new List<double>();

                for (var ptIdx = 0; ptIdx < setModel.Columns.Count; ptIdx++)
                {
                    var colName = setModel.Columns[ptIdx];

                    if (firstRow)
                        dataDto.Labels.Add(colName);
                    else if (dataDto.Labels.Count <= ptIdx)
                        break;
                    else if (dataDto.Labels[ptIdx] != colName)
                        throw new ArgumentException($"Expected column '{dataDto.Labels[ptIdx]}', got '{colName}'.");

                    dataList.Add(setModel.Values[ptIdx]);
                }

                setDto.Data = dataList;
                firstRow = false;
            }
        }


        private static void AddXyData(ChartDataDto dataDto, ChartDataModel model)
        {
            foreach (var setModel in model.DataSets)
            {
                var setDto = CreateDatasetDto(setModel);
                dataDto.Datasets.Add(setDto);
                var dataList = new List<CoordinatesDto>();

                for (var ptIdx = 0; ptIdx < setModel.Columns.Count; ptIdx++)
                {
                    if (!double.TryParse(setModel.Columns[ptIdx], out var x))
                        throw new ArgumentException($"Expected a number, got '{setModel.Columns[ptIdx]}'.");

                    var y = setModel.Values[ptIdx];
                    dataList.Add(new CoordinatesDto { X = x, Y = y });
                }

                setDto.Data = dataList;
            }
        }


        private static void AddXyzData(ChartDataDto dataDto, ChartDataModel model)
        {
            // Find the scaling range for the radius, so r = a * depth + b
            const int minR = 2;
            const int maxR = 50;
            var minZ = model.DataSets.SelectMany(d => d.Depths).Min();
            var maxZ = model.DataSets.SelectMany(d => d.Depths).Max();
            var hasDepth = !Utility.AlmostEqual(minZ, maxZ);
            var a = hasDepth ? (maxR - minR) / (maxZ - minZ) : 0;
            var b = -minZ * a + minR;

            // Create the data items
            foreach (var setModel in model.DataSets)
            {
                var setDto = CreateDatasetDto(setModel);
                dataDto.Datasets.Add(setDto);
                var dataList = new List<BubbleDto>();

                for (var ptIdx = 0; ptIdx < setModel.Columns.Count; ptIdx++)
                {
                    if (!double.TryParse(setModel.Columns[ptIdx], out var x))
                        throw new ArgumentException($"Expected a number, got '{setModel.Columns[ptIdx]}'.");

                    var y = setModel.Values[ptIdx];
                    var z = setModel.Depths[ptIdx] * a + b;
                    dataList.Add(new BubbleDto { X = x, Y = y, R = (int)z });
                }

                setDto.Data = dataList;
            }
        }


        private static ChartDataSetDto CreateDatasetDto(ChartDataSetModel setModel)
        {
            return new ChartDataSetDto
            {
                Label = setModel.Label,
                BorderWidth = setModel.UseBorder ? 2 : 0,
                BorderColor = setModel.BorderColor,
                BackgroundColor = setModel.BackgroundColor.Select(c => c.ToJavaScript()).ToList()
            };
        }

        #endregion
    }
}
