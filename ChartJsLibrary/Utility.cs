﻿using System;


namespace ChartJsLibrary
{
    public static class Utility
    {
        public static bool AlmostEqual(double number1, double number2) => Math.Abs(number1 - number2) < 0.001;


        public static string ToCamelCase(this string str)
        {
            var head = char.ToLowerInvariant(str[0]);
            var tail = str.Substring(1);
            return $"{head}{tail}";
        }
    }
}
