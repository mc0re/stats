﻿using System;


namespace ChartJsLibrary
{
    public sealed class ColorModeAttribute : Attribute
    {
        public ColorModes ColorMode { get; }


        public double Alpha { get; }


        public ColorModeAttribute(ColorModes mode, double alpha)
        {
            ColorMode = mode;
            Alpha = alpha;
        }
    }
}