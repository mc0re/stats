﻿using System.Collections.Generic;


namespace ChartJsLibrary
{
    /// <summary>
    /// Represents a set of data points, which have something in common.
    /// </summary>
    public class ChartDataSetModel
    {
        #region Properties

        /// <summary>
        /// Data set title; shown only on some chart types.
        /// </summary>
        public string Label { get; internal set; }


        /// <summary>
        /// Categories, to which the data points belong, or X-coordinates of the points.
        /// </summary>
        public List<string> Columns { get; } = new List<string>();


        /// <summary>
        /// Data values or Y-coordinates.
        /// </summary>
        public List<double> Values { get; } = new List<double>();


        /// <summary>
        /// List of Z-coordinates; shown only on some chart types.
        /// </summary>
        public List<double> Depths { get; } = new List<double>();


        /// <summary>
        /// If only one colour, it is valid for the whole data set.
        /// If more, there should be one per each data point.
        /// </summary>
        public List<ColorModelRgb> BackgroundColor { get; internal set; }


        /// <summary>
        /// Whether to draw a border around the item.
        /// </summary>
        public bool UseBorder { get; internal set; }


        /// <summary>
        /// If the border is drawn, its colour.
        /// </summary>
        public string BorderColor { get; internal set; }

        #endregion


        #region API

        /// <summary>
        /// Add a categorized data item.
        /// </summary>
        public void AddPoint(string columnName, double value)
        {
            AddPointInternal(columnName, value);
        }


        /// <summary>
        /// Add an XY data item.
        /// </summary>
        public void AddPoint(double x, double y)
        {
            AddPointInternal(x.ToString(), y);
        }


        /// <summary>
        /// Add an XYZ data item.
        /// </summary>
        public void AddPoint(double x, double y, double z)
        {
            AddPointInternal(x.ToString(), y, z);
        }

        #endregion


        #region Utility

        private void AddPointInternal(string column, double value, double depth = 0)
        {
            Columns.Add(column);
            Values.Add(value);
            Depths.Add(depth);
        }

        #endregion
    }
}