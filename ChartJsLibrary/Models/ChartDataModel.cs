﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace ChartJsLibrary
{
    /// <summary>
    /// Represents the chart data as it is being built.
    /// </summary>
    public class ChartDataModel
    {
        #region Fields

        private bool mIsColorSet;

        #endregion


        #region Properties

        /// <summary>
        /// Chart title.
        /// </summary>
        public string Label { get; }


        /// <summary>
        /// Chart type.
        /// </summary>
        public ChartTypes Type { get; }


        /// <summary>
        /// One or more data sets to visualize.
        /// </summary>
        public List<ChartDataSetModel> DataSets { get; } = new List<ChartDataSetModel>();

        #endregion


        #region Init and clean-up

        /// <summary>
        /// Create a new chart object.
        /// </summary>
        /// <param name="type">Chart type</param>
        /// <param name="title">Chart title</param>
        public ChartDataModel(ChartTypes type, string title)
        {
            Type = type;
            Label = title;
        }

        #endregion


        #region API

        /// <summary>
        /// Add an empty data set with the given name.
        /// </summary>
        /// <remarks>
        /// Use <see cref="ChartDataSetModel.AddPoint"/> to add data.
        /// </remarks>
        public ChartDataSetModel AddSet(string name)
        {
            var ds = new ChartDataSetModel { Label = name };
            DataSets.Add(ds);

            return ds;
        }


        /// <summary>
        /// Set colours as defined by <see cref="ColorModeAttribute"/>.
        /// </summary>
        /// <remarks>Does not set the colours if already set.</remarks>
        public void SetDefaultColors(int startAngle = 40)
        {
            if (mIsColorSet) return;

            var cmAttr = Type.GetType().GetField(Type.ToString()).GetCustomAttribute<ColorModeAttribute>();

            switch (cmAttr.ColorMode)
            {
                case ColorModes.DataSet:
                    SetDataSetColors(startAngle, cmAttr.Alpha);
                    break;

                case ColorModes.Columns:
                    SetColumnColors(startAngle, cmAttr.Alpha);
                    break;
            }
        }


        /// <summary>
        /// Set one colour for each data set.
        /// </summary>
        /// <param name="startAngle">Starting angle (0..359) on the HSL wheel (0 = red)</param>
        /// <param name="alpha">
        /// Opacity (0..1) of the elements' background;
        /// border is the same colour as the background with opacity 1.
        /// </param>
        /// <remarks>
        /// Call after all data is created, so the number of items is known.
        /// </remarks>
        public void SetDataSetColors(int startAngle = 40, double alpha = 0.6)
        {
            var colors = ColorGenerator.Create(DataSets.Count, alpha, startAngle);

            for (var dsIdx = 0; dsIdx < DataSets.Count; dsIdx++)
            {
                DataSets[dsIdx].BackgroundColor = new List<ColorModelRgb> { colors[dsIdx] };
                DataSets[dsIdx].UseBorder = true;
                DataSets[dsIdx].BorderColor = colors[dsIdx].SetAlpha(1).ToJavaScript();
            }

            mIsColorSet = true;
        }


        /// <summary>
        /// Set different colours for each column, but the same across data sets.
        /// </summary>
        /// <param name="startAngle">Starting angle (0..359) on the HSL wheel (0 = red)</param>
        /// <param name="alpha">
        /// Opacity (0..1) of the elements' background;
        /// border is the same colour as the background with opacity 1.
        /// </param>
        /// <remarks>
        /// Call after all data is created, so the number of items is known.
        /// </remarks>
        public void SetColumnColors(int startAngle = 40, double alpha = 0.7)
        {
            var colors = ColorGenerator.Create(DataSets[0].Columns.Count, alpha, startAngle);
            var borderColor = new ColorModelRgb { Alpha = 1 };

            for (var dsIdx = 0; dsIdx < DataSets.Count; dsIdx++)
            {
                DataSets[dsIdx].BackgroundColor = colors.ToList();
                DataSets[dsIdx].UseBorder = true;
                DataSets[dsIdx].BorderColor = borderColor.ToJavaScript();
            }

            mIsColorSet = true;
        }


        /// <summary>
        /// Set different colours for each data item.
        /// </summary>
        /// <param name="startAngle">Starting angle (0..359) on the HSL wheel (0 = red)</param>
        /// <param name="alpha">
        /// Opacity (0..1) of the elements' background;
        /// border is the same colour as the background with opacity 1.
        /// </param>
        /// <remarks>
        /// Call after all data is created, so the number of items is known.
        /// </remarks>
        public void SetIndividualColors(int startAngle = 40, double alpha = 0.7)
        {
            var colors = ColorGenerator.Create(DataSets.Sum(d => d.Columns.Count), alpha, startAngle);
            var borderColor = new ColorModelRgb { Alpha = 1 };

            for (var dsIdx = 0; dsIdx < DataSets.Count; dsIdx++)
            {
                var count = DataSets[dsIdx].Columns.Count;
                DataSets[dsIdx].BackgroundColor = colors.Take(count).ToList();
                DataSets[dsIdx].UseBorder = true;
                DataSets[dsIdx].BorderColor = borderColor.ToJavaScript();
                colors = colors.Skip(count).ToList();
            }

            mIsColorSet = true;
        }

        #endregion
    }
}
