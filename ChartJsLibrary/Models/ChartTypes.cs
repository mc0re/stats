﻿namespace ChartJsLibrary
{
    public enum ChartTypes
    {
        // Categorized

        [ColorMode(ColorModes.DataSet, 0.6)]
        Bar,

        [ColorMode(ColorModes.Columns, 0.7)]
        Pie,

        [ColorMode(ColorModes.DataSet, 0.4)]
        Radar,

        [ColorMode(ColorModes.Columns, 0.7)]
        PolarArea,

        [ColorMode(ColorModes.DataSet, 0.1)]
        Line,

        // XY

        [ColorMode(ColorModes.DataSet, 0.7)]
        Scatter,

        // XYZ

        [ColorMode(ColorModes.DataSet, 0.4)]
        Bubble
    }
}