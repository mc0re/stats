﻿namespace ChartJsLibrary
{
    /// <summary>
    /// Describes the default colorization mode for different charts.
    /// </summary>
    public enum ColorModes
    {
        /// <summary>
        /// One colour per data set.
        /// </summary>
        DataSet,


        /// <summary>
        /// Different colours for each column, but the same across data sets.
        /// </summary>
        Columns
    }
}