﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ChartJsLibrary
{
    /// <summary>
    /// When writing, convert a list of one element to a single value,
    /// a list with multiple elements to an array.
    /// </summary>
    /// <typeparam name="T">List item type</typeparam>
    public class SingleOrArrayConverter<T> : JsonConverter<object>
    {
        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert == typeof(List<T>);
        }


        public override object Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }


        public override void Write(Utf8JsonWriter writer, object value, JsonSerializerOptions options)
        {
            if (!(value is List<T> list))
                throw new ArgumentException($"Expected List<{typeof(T).Name}, got {value.GetType()}.");

            if (list.Count == 1)
            {
                writer.WriteStringValue(list[0].ToString());
            }
            else
            {
                writer.WriteStartArray();

                foreach (var item in list)
                {
                    writer.WriteStringValue(item.ToString());
                }

                writer.WriteEndArray();
            }
        }
    }
}