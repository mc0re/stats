﻿using System.Collections.Generic;


namespace ChartJsLibrary
{
    /// <summary>
    /// Class for serialization.
    /// </summary>
    public class ChartDataDto
    {
        public string Type { get; set; }

        public string Label { get; set; }

        public List<string> Labels { get; } = new List<string>();

        public List<ChartDataSetDto> Datasets { get; } = new List<ChartDataSetDto>();
    }
}
