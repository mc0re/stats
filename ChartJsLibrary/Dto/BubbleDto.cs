﻿namespace ChartJsLibrary
{
    public class BubbleDto : CoordinatesDto
    {
        /// <summary>
        /// Radius in pixels.
        /// </summary>
        public int R { get; set; }
    }
}