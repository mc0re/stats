﻿using System.Collections.Generic;
using System.Text.Json.Serialization;


namespace ChartJsLibrary
{
    /// <summary>
    /// Class for serialization.
    /// </summary>
    public class ChartDataSetDto
    {
        public string Label { get; set; }


        /// <summary>
        /// To allow both List of double and List of <see cref="CoordinatesDto"/> and other lists.
        /// </summary>
        public object Data { get; set; }


        [JsonConverter(typeof(SingleOrArrayConverter<string>))]
        public List<string> BackgroundColor { get; set; }


        public string BorderColor { get; set; }


        public int BorderWidth { get; set; }
    }
}