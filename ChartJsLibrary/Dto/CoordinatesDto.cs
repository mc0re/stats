﻿namespace ChartJsLibrary
{
    public class CoordinatesDto
    {
        public double X { get; set; }

        public double Y { get; set; }
    }
}