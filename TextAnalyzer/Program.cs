﻿namespace TextAnalyzer
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            //Examples.CompareCharFrequencies("Paradise", "Alice", "Harry", "Twitter");
            //Examples.CompareWordLengths("Paradise", "Alice", "Harry", "Twitter");
            //Examples.CompareSentenceLengths(new[] { "Paradise", "Alice", "Harry" }, new[] { "Twitter" });
            //Examples.CompareWordClasses("Paradise", "Alice", "Harry", "Twitter");
            Examples.CompareWordSentiments("Paradise", "Alice", "Harry", "Twitter");
        }
    }
}
