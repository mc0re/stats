﻿using ChartJsLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TextData;

namespace TextAnalyzer
{
    public class Examples
    {
        public static void CompareCharFrequencies(params string[] names)
        {
            var model = new ChartDataModel(ChartTypes.Bar, "Chars in texts");

            foreach (var name in names)
            {
                var text = File.ReadAllText($"Texts/{name}.txt");
                var countByChar = (
                    from c in text
                    where TextProcessor.IsWordChar(c)
                    let key = TextProcessor.ToLower(c)
                    group c by key into g
                    select new
                    {
                        Char = g.Key,
                        Count = g.Count()
                    }
                    ).ToList();

                double allChars = countByChar.Sum(c => c.Count);
                var freqByChar = countByChar.ToDictionary(e => e.Char, e => e.Count / allChars);

                var set = model.AddSet(name);
                foreach (var ch in "abcdefghijklmnopqrstuvwxyz")
                {
                    if (!freqByChar.TryGetValue(ch, out var freq))
                        freq = 0;

                    set.AddPoint(ch.ToString(), freq);
                }
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }


        public static void CompareWordLengths(params string[] names)
        {
            var freqByDoc = new Dictionary<string, IDictionary<int, double>>();

            foreach (var name in names)
            {
                var text = File.ReadAllText($"Texts/{name}.txt");
                var countByLength = (
                    from w in TextProcessor.ExtractWords(text)
                    group w by w.Length into g
                    select new
                    {
                        Length = g.Key,
                        Count = g.Count()
                    }
                    ).ToList();

                double allWords = countByLength.Sum(w => w.Count);
                freqByDoc.Add(name, countByLength.ToDictionary(s => s.Length, s => s.Count / allWords));
            }

            var lengths = freqByDoc.Values
                .SelectMany(s => s.Keys)
                .Distinct()
                .OrderBy(a => a)
                .Where(a => a <= 20)
                .ToList();

            var model = new ChartDataModel(ChartTypes.Line, "Word lengths");

            foreach (var name in names)
            {
                var set = model.AddSet(name);
                var counters = freqByDoc[name];

                foreach (var len in lengths)
                {
                    if (!counters.TryGetValue(len, out var count))
                        count = 0;

                    set.AddPoint(len, count);
                }
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }


        public static void CompareSentenceLengths(string[] texts, string[] oneLiners)
        {
            var freqByDoc = new Dictionary<string, IDictionary<int, double>>();
            var names = texts.Concat(oneLiners);

            foreach (var name in names)
            {
                var text = File.ReadAllText($"Texts/{name}.txt");
                var counters = (
                    from s in TextProcessor.ExtractSentences(text, oneLiners.Contains(name))
                    group s by TextProcessor.ExtractWords(s).Count into g
                    select new
                    {
                        WordCount = g.Key,
                        Count = g.Count()
                    }
                    ).ToList();
                double allSentences = counters.Sum(c => c.Count);
                freqByDoc.Add(name, counters.ToDictionary(c => c.WordCount, c => c.Count / allSentences));
            }

            var lengths = freqByDoc.Values
                .SelectMany(s => s.Keys)
                .Distinct()
                .OrderBy(a => a)
                .Where(a => a <= 100)
                .ToList();

            var model = new ChartDataModel(ChartTypes.Line, "Sentence lengths");

            foreach (var name in names)
            {
                var set = model.AddSet(name);
                var counters = freqByDoc[name];

                foreach (var len in lengths)
                {
                    if (!counters.TryGetValue(len, out var count))
                        count = 0;

                    set.AddPoint(len, count);
                }
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }


        public static void CompareWordClasses(params string[] names)
        {
            var wc = new WordClass();
            var freqByDoc = new Dictionary<string, IDictionary<WordClasses, double>>();

            foreach (var name in names)
            {
                var text = File.ReadAllText($"Texts/{name}.txt");
                var countByClass = (
                    from w in TextProcessor.ExtractWords(text)
                    group w by wc.GetWordClass(w) into g
                    select new
                    {
                        WordClass = g.Key,
                        Count = g.Count()
                    }
                    ).ToList();
                double allWords = countByClass.Sum(c => c.Count);
                freqByDoc.Add(name, countByClass.ToDictionary(c => c.WordClass, c => c.Count / allWords));
            }
            var classes = freqByDoc.Values
                .SelectMany(s => s.Keys)
                .Distinct()
                .Where(a => a != WordClasses.Unknown)
                .OrderBy(a => a)
                .ToList();

            var model = new ChartDataModel(ChartTypes.Radar, "Word classes");

            foreach (var name in names)
            {
                var set = model.AddSet(name);
                var counters = freqByDoc[name];

                foreach (var cl in classes)
                {
                    if (!counters.TryGetValue(cl, out var count))
                        count = 0;

                    set.AddPoint(cl.ToString(), count);
                }
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }


        public static void CompareWordSentiments(params string[] names)
        {
            var sentiment = new Sentiment();
            var freqByDoc = new Dictionary<string, IDictionary<Polarities, double>>();

            foreach (var name in names)
            {
                var text = File.ReadAllText($"Texts/{name}.txt");
                var countByPolarity = (
                    from w in TextProcessor.ExtractWords(text)
                    group w by sentiment.GetPolarity(w) into g
                    select new
                    {
                        Polarity = g.Key,
                        Count = g.Count()
                    }
                    ).ToList();
                double allWords = countByPolarity.Sum(c => c.Count);
                freqByDoc.Add(name, countByPolarity.ToDictionary(c => c.Polarity, c => c.Count / allWords));
            }

            var model = new ChartDataModel(ChartTypes.Bar, "Polarities");
            var set = model.AddSet("Positive");

            foreach (var name in names)
            {
                var counters = freqByDoc[name];
                set.AddPoint(name, counters[Polarities.Positive]);
            }

            var str = ChartJs.Serialize(model);
            TextCopy.Clipboard.SetText(str);
            Console.WriteLine("Copied.");
        }
    }
}
