﻿using System;
using System.Collections.Generic;
using System.Globalization;


namespace TextAnalyzer
{
    /// <summary>
    /// Utility for simple text processing.
    /// </summary>
    public static class TextProcessor
    {
        private static CultureInfo mCulture = CultureInfo.InvariantCulture;


        #region API

        /// <summary>
        /// A character is a part of a word if it is a letter or an apostrophe.
        /// </summary>
        public static bool IsWordChar(char c) =>
            char.IsLetter(c) || c == '\'' || c == '’';


        /// <summary>
        /// A character ends a sentence if it's a specific punctuation,
        /// or a new line (when requested).
        /// </summary>
        public static bool IsSentenceEndChar(char c, bool newLineIsEos)
        {
            if (c == '.' || c == '?' || c == '!')
                return true;

            if (newLineIsEos && Environment.NewLine.Contains(c))
                return true;

            return false;
        }


        /// <summary>
        /// Convert a character to lower-case.
        /// </summary>
        public static char ToLower(char c) =>
            char.ToLower(c, mCulture);


        /// <summary>
        /// Split the text into words.
        /// </summary>
        public static List<string> ExtractWords(string text)
        {
            var res = new List<string>();

            var startIdx = 0;

            while (startIdx < text.Length)
            {
                while (startIdx < text.Length && !IsWordChar(text[startIdx]))
                    startIdx++;

                if (startIdx >= text.Length)
                    break;

                var endIdx = startIdx;

                while (endIdx + 1 < text.Length && IsWordChar(text[endIdx + 1]))
                    endIdx++;

                var word = text.Substring(startIdx, endIdx - startIdx + 1);
                res.Add(word);

                startIdx = endIdx + 1;
            }

            return res;
        }


        /// <summary>
        /// Split the text into sentences.
        /// </summary>
        public static List<string> ExtractSentences(string text, bool newLineIsEos)
        {
            var res = new List<string>();

            var startIdx = 0;

            while (startIdx < text.Length)
            {
                while (startIdx < text.Length && !IsWordChar(text[startIdx]))
                    startIdx++;

                if (startIdx >= text.Length)
                    break;

                var endIdx = startIdx + 1;

                while (endIdx < text.Length && !IsSentenceEndChar(text[endIdx], newLineIsEos))
                    endIdx++;

                if (endIdx < text.Length) endIdx++;
                var sentence = text.Substring(startIdx, endIdx - startIdx);
                res.Add(sentence);

                startIdx = endIdx + 1;
            }

            return res;
        }

        #endregion
    }
}